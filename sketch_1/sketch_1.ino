#define relay_0 7
#define relay_1 6
#define relay_2 5
#define relay_3 4
#define inputPulsePin 3
#define pulsedl 10          //quanti impulsi corrispondono ad un decilitro d' acqua
bool statebtn = 0;
int pipepulsecount[4];  // l' indice fa riferimento al tubo;
float pipelitercount[4];
char selectedpipeindex = 0;
int incomingByte = 0;
char i;


void setup() {

  pinMode(inputPulsePin, INPUT_PULLUP);
  pinMode(relay_0, OUTPUT);
  pinMode(relay_1, OUTPUT);
  pinMode(relay_2, OUTPUT);
  pinMode(relay_3, OUTPUT);

  attachInterrupt(digitalPinToInterrupt(inputPulsePin), doIncrement, DEC);
  Serial.begin (9600);
}



void loop() {

  if (Serial.available() > 0) {

    incomingByte = Serial.read(); // read the incoming byte:
    switch ((char)incomingByte) {
      case '0':                //chiude acqua
      waterclose();
      break;
      case '1':               //seleziona tubo 1
        selectpipe(0);
        break;
      case '2':
        selectpipe(1);     //seleziona tubo 2
        break;
      case '3':                //seleziona tubo 3
        selectpipe(2);
        break;
      case '4':                //seleziona tubo 4
        selectpipe(3);
        break;

      case 'l':               //legge quantità di acqua distribuite
        readliter();

        break;

      case 'r':                //azzerra quantità distribuite
        resetliter();
        break;

      default:
        break;

    }

  }

}


void doIncrement() {
  pipepulsecount[selectedpipeindex]++;
  if (pipepulsecount[selectedpipeindex] % pulsedl == 0) {
    pipelitercount[selectedpipeindex] += 0.1;
    pipepulsecount[selectedpipeindex] = 0;
  }
}


void selectpipe(char index) {
  selectedpipeindex = index;
  valvleon(index);
  Serial.print("Selezionato tubo ");
  Serial.println(index+1);
}


void readliter() {
  for (i = 0; i < 4; i++) {
    Serial.println("Litri di acqua distribuiti:");
    Serial.print("Tubo ");
    Serial.print(i + 1);
    Serial.print(":\t");
    Serial.println(pipelitercount[i] * 1);
  }
}


void resetliter() {
  for (i = 0; i < 4; i++) {
    pipelitercount[i] = 0;
    pipepulsecount[i] = 0;
  }
}

void waterclose(){
   digitalWrite(relay_0, LOW);
  digitalWrite(relay_1, LOW);
  digitalWrite(relay_2, LOW);
  digitalWrite(relay_3, LOW);
  }

void valvleon(char index) {
  //spengo prima tutte le valvole
  waterclose();

  //attivo la valvola richiesta

  switch (index) {
    case 0:
      digitalWrite(relay_0, HIGH);
      break;

    case 1:
      digitalWrite(relay_1, HIGH);
      break;

    case 2:
      digitalWrite(relay_2, HIGH);
      break;

    case 3:
      digitalWrite(relay_3, HIGH);
      break;

    default:
      break;

  }
}

