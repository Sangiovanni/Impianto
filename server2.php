<?php

define("PORT", "/dev/ttymxc3");
//define("PORT", "/dev/ttyACM0");

// prevent the server from timing out
set_time_limit(0);

require 'websocket/class.PHPWebSocket.php';
require 'php_serial.class.php';

$serial = NULL; //istanza oggetto porta seriale

// when a client sends data to the server
function wsOnMessage($clientID, $message, $messageLength, $binary) {
    global $Server;
    global $serial;
  // $Server->wsSend($clientID, "messaggio ricevuto: ". $message);
   $serial->sendMessage($message);
   $read = $serial->readPort();    //leggo risposta arduino
   $Server->wsSend($clientID, $read); 
}


// when a client connects
function wsOnOpen($clientID)
{
	global $Server;
        
        $Server->wsSend($clientID,"Attendere connessione dispositivo...");
        
        serialInit();
        if(serialOpen()){
            $Server->wsSend($clientID,"connessione col dispositivo stabilita.");
        }else{
            $Server->wsSend($clientID,"Impossibile connettersi al dispositivo.");
        }
        
}

function wsOnClose($clientID, $status) {
	global $Server;
        global $serial;
        $serial->deviceClose();
        
        
}

// start the server
$Server = new PHPWebSocket();
$Server->bind('message', 'wsOnMessage');
$Server->bind('open', 'wsOnOpen');
$Server->bind('close', 'wsOnClose');
// for other computers to connect, you will probably need to change this to your LAN IP or external IP,
// alternatively use: gethostbyaddr(gethostbyname($_SERVER['SERVER_NAME']))
$Server->wsStartServer('127.0.0.1', 9300);


//php serial Port

function serialInit(){
    global $serial;
    $serial = new phpSerial;
    $serial->deviceSet(PORT);
    $serial->confBaudRate(9600);
    $serial->confParity("none");
    $serial->confStopBits(1);
    $serial->confFlowControl("none");
}

function serialOpen(){
    global $serial;
   $issue = false;
   if($serial->deviceOpen()){
       $issue=true;
       sleep(10);
   }
   return $issue;
}


