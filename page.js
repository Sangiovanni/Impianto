$(document).ready(function () {

    ///////////  WEBSOCKET  //////////////

    var Server = new FancyWebSocket('ws://127.0.0.1:9300');


    function send(text) {
        Server.send('message', text);
    }

    Server.bind('open', function () {
        $("#message").html("Connesso al Server.");
    });

    //OH NOES! Disconnection occurred.
    Server.bind('close', function (data) {
        $("#message").html("Disconnesso dal Server.");
    });

    //Log any messages sent from server
    Server.bind('message', function (payload) {
        $("#message").html(payload);
    });

    Server.connect();


    //////////////////////////////////////

    $(".btn-group button").click(function () {
        var id = $(this).attr("id");

        var previd = parseInt(id) - 1;
        var nextid = parseInt(id) + 1;

        var cls = $(this).attr("class");

        if (id == "1" || id == "3" || id == "5" || id == "7") {   //btn On

            //rimuovo eventuali altri pulsanti attivati
            doAllBtnOff();

            $("#" + id).removeClass("btn-default").addClass("btn-success");

            $("#" + nextid).removeClass("btn-danger").addClass("btn-default");

            /*
             //rimuovo eventuali altri pulsanti attivati
             $(".btn-success").each(function () {
             if ($(this).attr("id") != id) {
             var nextid = parseInt($(this).attr("id")) + 1;
             $(this).removeClass("btn-success").addClass("btn-default");
             $("#" + nextid).removeClass("btn-default").addClass("btn-danger");
             }
             });
             
             
             */
            var pipe = Math.floor(parseInt(id) / 2) + 1;

            send(pipe);
            
            doBtnOn(pipe);  //rimuovere successivamente
            

        } else {  //btn Off
            
            if($("#"+previd).hasClass("btn-success")){
                send(0); //spegne tutte le valvole
                doAllBtnOff();   //rimuovere successivamente
            }
            
           
            
            
            
            if (cls == "btn-default") {
                $(this).removeClass("btn-default").addClass("btn-danger");
                $("#" + previd).removeClass("btn-success").addClass("btn-default");
            }
            send(0);
        }


    });

    $("#rst").click(function () { //reset litri distribuiti
        send('r');
    })



    function doBtnOn(pipe) {
        var btnid = pipe * 2 - 1;
        var nextid = parseInt(btnid) + 1;

        $("#" + btnid).removeClass("btn-default").addClass("btn-success");

        $("#" + nextid).removeClass("btn-danger").addClass("btn-default");

    }

    

    function doAllBtnOff() {
        for (var btnid = 1; btnid <= 7; btnid = btnid + 2) {
            if ($("#" + btnid).hasClass("btn-success")) {
                $("#" + btnid).removeClass("btn-success").addClass("btn-default");
                var nextid = btnid + 1;
                $("#" + nextid).removeClass("btn-default").addClass("btn-danger");
            }
        }
    }

});